package com.example.novelspringbootmysql.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Chapter {
    private Long id;

    private String fictionId;

    private String chapterTitle;

    private Long contentId;

}