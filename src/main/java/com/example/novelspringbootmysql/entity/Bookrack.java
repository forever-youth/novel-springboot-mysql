package com.example.novelspringbootmysql.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bookrack {
    private Long id;

    private String userId;

    private String fictionId;

    private Date createTime;

    private Date updateTime;

    private String readStatus;

    private Integer lstop;

    private Integer fakeDelete;
}