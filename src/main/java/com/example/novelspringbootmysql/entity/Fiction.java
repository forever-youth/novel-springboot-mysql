package com.example.novelspringbootmysql.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Fiction {
    private Long id;

    private String fictionId;

    private String fictionName;

    private String authorId;

    private String fictionDescription;

    private String coverPath;

    private String typeId;

    private String wordCount;

    private String fictionState;
}