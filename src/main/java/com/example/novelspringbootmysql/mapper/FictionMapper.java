package com.example.novelspringbootmysql.mapper;

import com.example.novelspringbootmysql.entity.Fiction;
import java.util.List;

public interface FictionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Fiction record);

    Fiction selectByPrimaryKey(Long id);

    Fiction selectByFictionId(String fictionId);

    List<Fiction> selectAll();

    int updateByPrimaryKey(Fiction record);

    List<Fiction> findByTypeId(String typeId);

    List<Fiction> selectLikeFictionName(String fictionName);
}