package com.example.novelspringbootmysql.mapper;

import com.example.novelspringbootmysql.entity.Type;
import java.util.List;

public interface TypeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Type record);

    Type selectByPrimaryKey(Long id);

    List<Type> selectAll();

    int updateByPrimaryKey(Type record);

    Type selectByTypeId(String typeId);
}