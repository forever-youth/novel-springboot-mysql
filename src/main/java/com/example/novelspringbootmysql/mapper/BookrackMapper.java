package com.example.novelspringbootmysql.mapper;

import com.example.novelspringbootmysql.entity.Bookrack;
import com.example.novelspringbootmysql.vo.BookRackVo;

import java.util.List;

public interface BookrackMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Bookrack record);

    Bookrack selectByPrimaryKey(Long id);

    List<Bookrack> selectAll();

    int updateByPrimaryKey(Bookrack record);

    List<BookRackVo> findBookRackByUserId(String userId);

    int findBookRackByUserIdOne(String userId,String fictionId);
}