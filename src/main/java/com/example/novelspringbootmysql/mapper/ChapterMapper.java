package com.example.novelspringbootmysql.mapper;

import com.example.novelspringbootmysql.entity.Chapter;
import java.util.List;

public interface ChapterMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Chapter record);

    Chapter selectByPrimaryKey(Long id);

    List<Chapter> selectAll();

    int updateByPrimaryKey(Chapter record);

    List<Chapter> selectByFictionId(String fictionId);

    Chapter selectOneByFictionId(String fictionId);
}