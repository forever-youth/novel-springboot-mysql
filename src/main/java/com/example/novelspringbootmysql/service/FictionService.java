package com.example.novelspringbootmysql.service;

import com.example.novelspringbootmysql.entity.Fiction;

import java.util.List;

public interface FictionService {
    List<Fiction> selectAll();

    List<Fiction> findByTypeId(String typeId);

    Fiction selectByPrimaryKey(Long id);

    Fiction selectByFictionId(String fictionId);

    List<Fiction> selectLikeFictionName(String fictionName);


}
