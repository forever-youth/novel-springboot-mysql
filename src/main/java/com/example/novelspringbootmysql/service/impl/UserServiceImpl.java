package com.example.novelspringbootmysql.service.impl;

import com.example.novelspringbootmysql.entity.User;
import com.example.novelspringbootmysql.mapper.UserMapper;
import com.example.novelspringbootmysql.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }

    @Override
    public int insert(User record) {
        return userMapper.insert(record);
    }

    @Override
    public String selectByMaxId() {
        return userMapper.selectByMaxId();
    }
}
