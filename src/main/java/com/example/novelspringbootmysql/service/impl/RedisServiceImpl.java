package com.example.novelspringbootmysql.service.impl;

import com.example.novelspringbootmysql.redis.RedisCommons;
import com.example.novelspringbootmysql.redis.RedisUtil;
import com.example.novelspringbootmysql.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void set(String key, String value) {
        redisUtil.set(key,value);
    }

    @Override
    public Object get(String key) {
        return redisUtil.get(key);
    }

    @Override
    public Set<String> getKeys(String pattern) {
        return redisUtil.getKeys(pattern);
    }

    @Override
    public List<Map.Entry<String, Integer>> getKeysMapRackTop() {
        String newKey =  RedisCommons.RACK + "*";
        Set<String> keySet = redisUtil.getKeys(newKey);
        TreeMap<String,Integer> map = new TreeMap<String, Integer>();
        for (String key: keySet) {
            Object obj = redisUtil.get(key);
            map.put(key, Integer.valueOf((String) obj));
        }
        List<Map.Entry<String, Integer>> treeMapList = new ArrayList<Map.Entry<String, Integer>>(map.entrySet());
        Collections.sort(treeMapList, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o2.getValue() - o1.getValue());
            }
        });
        return treeMapList;
    }

    @Override
    public void incr(String key) {
        redisUtil.incr(key);
    }

    @Override
    public void setZSetOps(String key, String value, int score) {
        redisUtil.setZSetOps(key,value,score);
    }

    @Override
    public Set<Object> getZSetOpsReverseRange(String key, long start, long end) {
        return redisUtil.getZSetOpsReverseRange(key,start,end);
    }

    @Override
    public Double getZSetOpsScore(String key, String value) {
        return redisUtil.getZSetOpsScore(key,value);
    }

}
