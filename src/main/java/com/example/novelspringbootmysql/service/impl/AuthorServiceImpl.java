package com.example.novelspringbootmysql.service.impl;

import com.example.novelspringbootmysql.entity.Author;
import com.example.novelspringbootmysql.mapper.AuthorMapper;
import com.example.novelspringbootmysql.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorMapper authorMapper;

    @Override
    public Author selectByAuthorId(String authorId){
        return authorMapper.selectByAuthorId(authorId);
    }
}
