package com.example.novelspringbootmysql.service.impl;

import com.example.novelspringbootmysql.entity.Content;
import com.example.novelspringbootmysql.mapper.ContentMapper;
import com.example.novelspringbootmysql.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContentServiceImpl implements ContentService {

    @Autowired
    private ContentMapper contentMapper;

    @Override
    public int insert(List<String> records) {
        Content content;
        long id = 3184;
        for (String record: records) {
            content = new Content(id,record);
            contentMapper.insert(content);
            id++;
        }
        return 0;
    }

    @Override
    public Content selectByPrimaryKey(Long id) {
        return contentMapper.selectByPrimaryKey(id);
    }
}
