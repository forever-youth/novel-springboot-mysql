package com.example.novelspringbootmysql.service.impl;

import com.example.novelspringbootmysql.entity.Type;
import com.example.novelspringbootmysql.mapper.TypeMapper;
import com.example.novelspringbootmysql.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeMapper typeMapper;

    @Override
    public List<Type> selectAll() {
        return typeMapper.selectAll();
    }

    @Override
    public Type selectByTypeId(String typeId) {
        return typeMapper.selectByTypeId(typeId);
    }
}
