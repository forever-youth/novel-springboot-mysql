package com.example.novelspringbootmysql.service.impl;

import com.example.novelspringbootmysql.entity.Fiction;
import com.example.novelspringbootmysql.mapper.FictionMapper;
import com.example.novelspringbootmysql.service.FictionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FictionServiceImpl implements FictionService {

    @Autowired
    private FictionMapper fictionMapper;

    @Override
    public List<Fiction> selectAll() {
        return fictionMapper.selectAll();
    }

    @Override
    public List<Fiction> findByTypeId(String typeId) {
        return fictionMapper.findByTypeId(typeId);
    }

    @Override
    public Fiction selectByPrimaryKey(Long id) {
        return null;
    }

    @Override
    public Fiction selectByFictionId(String fictionId) {
        return fictionMapper.selectByFictionId(fictionId);
    }

    @Override
    public List<Fiction> selectLikeFictionName(String fictionName) {
        return fictionMapper.selectLikeFictionName(fictionName);
    }
}
