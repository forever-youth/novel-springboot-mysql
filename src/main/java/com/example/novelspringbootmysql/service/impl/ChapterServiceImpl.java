package com.example.novelspringbootmysql.service.impl;

import com.example.novelspringbootmysql.entity.Chapter;
import com.example.novelspringbootmysql.mapper.ChapterMapper;
import com.example.novelspringbootmysql.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ChapterServiceImpl implements ChapterService {

    @Autowired
    private ChapterMapper chapterMapper;

    /**
     * 改进：
     * id动态
     * fictionId动态
     * @param records
     * @return
     */
    @Override
    public int insert(List<String> records) {
        Chapter chapter;
        long id = 1;
        String fictionId = "600003";
        for (String record: records) {
            chapter= new Chapter(id,fictionId,record,id);
            chapterMapper.insert(chapter);
            id++;
        }
        return 0;
    }

    @Override
    public List<Chapter> selectByFictionId(String fictionId) {
        return chapterMapper.selectByFictionId(fictionId);
    }

    @Override
    public Chapter selectOneByFictionId(String fictionId) {
        return chapterMapper.selectOneByFictionId(fictionId);
    }

}
