package com.example.novelspringbootmysql.service.impl;

import com.example.novelspringbootmysql.entity.Bookrack;
import com.example.novelspringbootmysql.mapper.BookrackMapper;
import com.example.novelspringbootmysql.service.BookrackService;
import com.example.novelspringbootmysql.vo.BookRackVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookrackServiceImpl implements BookrackService {

    @Autowired
    private BookrackMapper bookrackMapper;

    @Override
    public List<BookRackVo> findBookRackByUserId(String userId) {
        return bookrackMapper.findBookRackByUserId(userId);
    }

    @Override
    public int findBookRackByUserIdOne(String userId, String fictionId) {
        return bookrackMapper.findBookRackByUserIdOne(userId,fictionId);
    }

    @Override
    public int insert(Bookrack record) {
        return bookrackMapper.insert(record);
    }
}
