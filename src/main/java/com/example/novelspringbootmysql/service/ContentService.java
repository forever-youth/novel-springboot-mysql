package com.example.novelspringbootmysql.service;

import com.example.novelspringbootmysql.entity.Content;

import java.util.List;

public interface ContentService {

    int insert(List<String> records);

    Content selectByPrimaryKey(Long id);

}
