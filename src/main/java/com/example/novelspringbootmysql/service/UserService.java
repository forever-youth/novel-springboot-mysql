package com.example.novelspringbootmysql.service;

import com.example.novelspringbootmysql.entity.User;

public interface UserService {
    User findByUsername(String username);

    int insert(User record);

    String selectByMaxId();
}
