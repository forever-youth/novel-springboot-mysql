package com.example.novelspringbootmysql.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RedisService {

    void set(String key, String value);

    Object get(String key);

    Set<String> getKeys(String pattern);

    List<Map.Entry<String, Integer>> getKeysMapRackTop();

    void incr(String key);

    void setZSetOps(String key, String value, int score);

    // 根据传入的key，start，end获取有序集合倒序（降序）的指定范围内的元素
    Set<Object> getZSetOpsReverseRange(String key, long start, long end);

    // 根据传入的key和value获取有序集合的指定成员的分数
    Double getZSetOpsScore(String key, String value);
}
