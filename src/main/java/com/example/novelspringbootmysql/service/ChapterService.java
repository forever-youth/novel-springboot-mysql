package com.example.novelspringbootmysql.service;

import com.example.novelspringbootmysql.entity.Chapter;

import java.util.List;

public interface ChapterService {

    int insert(List<String> records);

    List<Chapter> selectByFictionId(String fictionId);

    Chapter selectOneByFictionId(String fictionId);

}
