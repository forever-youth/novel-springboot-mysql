package com.example.novelspringbootmysql.service;

import com.example.novelspringbootmysql.entity.Author;

public interface AuthorService {

    Author selectByAuthorId(String authorId);

}
