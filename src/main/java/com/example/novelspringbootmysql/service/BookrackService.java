package com.example.novelspringbootmysql.service;

import com.example.novelspringbootmysql.entity.Bookrack;
import com.example.novelspringbootmysql.vo.BookRackVo;

import java.util.List;

public interface BookrackService {

    List<BookRackVo> findBookRackByUserId(String userId);

    int findBookRackByUserIdOne(String userId,String fictionId);

    int insert(Bookrack record);
}
