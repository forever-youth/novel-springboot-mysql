package com.example.novelspringbootmysql.service;

import com.example.novelspringbootmysql.entity.Type;

import java.util.List;

public interface TypeService {
    List<Type> selectAll();

    Type selectByTypeId(String typeId);
}
