package com.example.novelspringbootmysql.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsoupUtil {
    public static void main(String[] args) {
        List<String> list = inputUrlGetChapter("https://m.bqg337.com/book/25195/list.html");
        for (String str : list)  {
            System.out.println(str);
        }
    }

    /**
     * 获取小说的每一目录标题的内容
     */


    /**
     * 获取小说的全部目录
     * @param url https://www.23hh.com/book/24/24035/
     *            https://m.bqg337.com/book/25195/list.html
     * @return 目录的list
     */
    public static List<String> inputUrlGetChapter(String url) {
        List<String> list = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(url).get();
            //得到html页面上所有的a链接下的标题 第一章 xxxx
            String fileName = doc.select("dl>dd>a").text();
            //但是得到之后前面还有一些无关的数据，需要去除
            String fileNameNew = fileName.substring(fileName.indexOf("第一章"));
            //调用extractString方法，格式化字符串存入list数组
            list = extractString(fileNameNew);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    //字符串格式化 第一章xxx 第二章xxx
    private static List<String> extractString(String str) {
        List<String> result = new ArrayList<>();
        int start = 0;
        int end = str.indexOf(" 第", start + 1); // 找到第一个" 第"的位置
        while (end != -1) {
            String extractedString = str.substring(start, end); // 提取字符串
            result.add(extractedString.trim());
            start = end; // 更新start位置
            end = str.indexOf(" 第", start + 1); // 找到下一个" 第"的位置
        }
        // 处理最后一个字符串
        String lastString = str.substring(start).trim();
        result.add(lastString);
        return result;
    }


}
