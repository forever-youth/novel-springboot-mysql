package com.example.novelspringbootmysql;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.example.novelspringbootmysql.mapper")
@SpringBootApplication
public class NovelSpringbootMysqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(NovelSpringbootMysqlApplication.class, args);
    }

}
