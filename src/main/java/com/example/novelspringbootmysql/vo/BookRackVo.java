package com.example.novelspringbootmysql.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookRackVo {

    private String fictionId;

    private Date updateTime;

    private String readStatus;

    private Integer lstop;

    private Integer fakeDelete;

    private String fictionName;

    private String fictionState;

    private String typeName;

    private String authorName;

}
