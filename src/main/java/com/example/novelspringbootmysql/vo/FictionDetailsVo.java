package com.example.novelspringbootmysql.vo;

import com.example.novelspringbootmysql.entity.Chapter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FictionDetailsVo {
    private String fictionId;

    private String fictionName;

    private String authorId;

    private String fictionDescription;

    private String coverPath;

    private String typeId;

    private String wordCount;

    private String fictionState;


    private String typeName;

    private String authorName;

    private String authorDescription;

    private List<Chapter> chapters;
}
