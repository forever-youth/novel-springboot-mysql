package com.example.novelspringbootmysql.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchFictionVo {
    private String fictionId;

    private String fictionName;

    private String authorId;

    private String fictionDescription;

    private String coverPath;

    private String typeId;

    private String wordCount;

    private String fictionState;


    private String typeName;

    private String authorName;

    private String authorDescription;

    private String newContent;

    private Long contentId;
}
