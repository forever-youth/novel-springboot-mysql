package com.example.novelspringbootmysql.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/** 在properties中匹配相关的配置，然后再到RedisConfig中生成相关的factory，再到RedisUtil中操作相关数据
 * redisTemplate api
 * https://docs.spring.io/spring-data/redis/docs/current/api/org/springframework/data/redis/core/RedisTemplate.html
 */
@Component
public class RedisUtil {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * redis中的String的使用
     */
    // 存储数据  不设置时间
    public void set(String key, Object obj) {
         redisTemplate.opsForValue().set(key, obj);
    }

    // 存储数据  设置时间  单位为秒
    public void set(String key, Object obj, long timeout) {
        redisTemplate.opsForValue().set(key, obj, timeout, TimeUnit.SECONDS);
    }

    // 数据自增
    public void incr(String key) {
        redisTemplate.opsForValue().increment(key);
    }

    // 获取数据
    public Object get(String key) {
        if (!redisTemplate.hasKey(key)) {
            return null;
        }
        return redisTemplate.opsForValue().get(key);
    }

    // 删除数据
    public boolean delete(String key) {
        redisTemplate.delete(key);
        if (redisTemplate.hasKey(key)) {
            return false;
        }
        return true;
    }

    // 获取失效时间  -2 已经失效  -1 一直有效
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    public Set<String> getKeys(String pattern){
        return redisTemplate.keys(pattern);
    }

    /**
     * redis中的有序集合sorted set使用
     */
    // 向key这个集合中添加成员value，它的权重为score
    public void setZSetOps(String key,String value,int score){
        redisTemplate.opsForZSet().add(key,value,score);
    }

    // 根据传入的key，start，end获取有序集合倒序（降序）的指定范围内的元素
    public Set<Object> getZSetOpsReverseRange(String key, long start, long end) {
        return redisTemplate.opsForZSet().reverseRange(key, start, end);
    }

    // 根据传入的key和value获取有序集合的指定成员的分数
    public Double getZSetOpsScore(String key,String value){
        return redisTemplate.opsForZSet().score(key, value);
    }

}
