package com.example.novelspringbootmysql.redis;

public class RedisCommons {

    //加入书架榜前缀
    public static final String RACK = "rack_";

    //热度搜索榜前缀
    public static final String SEARCH = "search_";

    //热度搜索榜在redis中的key
    public static final String SEARCHKEY = "search";

    //热度收藏榜前缀
    public static final String TREASURE = "treasure_";

}
