package com.example.novelspringbootmysql.controller;

import com.example.novelspringbootmysql.entity.Bookrack;
import com.example.novelspringbootmysql.redis.RedisCommons;
import com.example.novelspringbootmysql.service.BookrackService;
import com.example.novelspringbootmysql.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;
import java.util.*;

@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    private RedisService redisService;

    @Autowired
    private BookrackService bookrackService;

    /**
     * 加入书架的榜单，redis存储
     */
    @GetMapping("/redisToRack")
    public String redisToRack(@RequestParam("fictionId") String fictionId,
                              @RequestParam("userId") String userId) {
        //判断是否加入书架，没有则加入书架,这里则不需要这样判断，因为已经加入的话发送不了链接
        Bookrack bookrack = new Bookrack(null,userId,fictionId,new Date(),new Date(),"未开始",0,1);
        bookrackService.insert(bookrack);

        String newKey =  RedisCommons.RACK + fictionId;
        Object obj = redisService.get(newKey);
        if (obj != null) {
            //redisService.incr(newKey);
            redisService.set(newKey, String.valueOf(Integer.valueOf((String) obj)+1));
            return "succeed+1";
        }
        String value = "1";
        redisService.set(newKey, value);
        return "succeed";
    }

    /**
     * 收藏榜单，加入收藏，redis存储
     * @return
     */
    @GetMapping("/redisToTreasure")
    public String redisToTreasure(){
        //创建一个收藏数据库
        //根据fictionId，userId加入收藏数据库

        //根据 RedisCommons.TREASURE + fictionId 以key存入redis，value值初始值为1
        //如果key存在则value+1，否则初始化

        return "succeed";
    }


}
